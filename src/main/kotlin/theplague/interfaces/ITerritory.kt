package theplague.interfaces

interface ITerritory {
    /**
     * List of icons of the current territory (4 max)
     */
    val icons:MutableList<Iconizable>
    fun iconList() : List<Iconizable>
    val position:Position
}