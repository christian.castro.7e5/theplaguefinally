package theplague.interfaces

interface Iconizable{
    val icon : String
}

class DefaultIcon :Iconizable{
    override val icon: String = ""
}