package theplague.logic.Weapons

import theplague.interfaces.Iconizable
import theplague.logic.Item

open class Weapon: Item() {
    override var timesLeft: Int = 5
    override val icon: String = " "
}