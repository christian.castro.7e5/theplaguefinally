package theplague.logic.Weapons

class Broom:Weapon() {
    override var timesLeft: Int = 5
    override val icon: String = "\uD83E\uDDF9"
}