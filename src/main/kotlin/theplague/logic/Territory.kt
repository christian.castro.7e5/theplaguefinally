package theplague.logic

import theplague.interfaces.ITerritory
import theplague.interfaces.Iconizable
import theplague.interfaces.Position

class Territory:ITerritory {

    override val icons = mutableListOf<Iconizable>()

    override fun iconList(): List<Iconizable> {
        return icons
    }
    override val position: Position = Position(0,0)
}