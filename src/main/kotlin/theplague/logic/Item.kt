package theplague.logic

import theplague.interfaces.Iconizable

abstract class Item: Iconizable {
    abstract var timesLeft:Int
}