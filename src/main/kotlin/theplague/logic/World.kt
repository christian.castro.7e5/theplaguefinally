package theplague.logic

import theplague.interfaces.*
import theplague.logic.Colonies.Ant
import theplague.logic.Colonies.Colonization
import theplague.logic.Colonies.Colony
import theplague.logic.Colonies.Dragon
import theplague.logic.Vehicles.Bicycle
import theplague.logic.Vehicles.Helicopter
import theplague.logic.Vehicles.OnFoot
import theplague.logic.Vehicles.Vehicle
import theplague.logic.Weapons.Broom
import theplague.logic.Weapons.Sword
import theplague.logic.Weapons.Weapon
import kotlin.math.abs
import kotlin.math.max


class World(
    override val width: Int = 7,
    override val height: Int = 7,
    override val player: IPlayer = Player()
) : IWorld {

    override val territories: List<List<Territory>> =
        List<List<Territory>>(height) { List<Territory>(width) { Territory() } }
    val colonies = mutableListOf<Colonization>()

    init{
        player.position = Position(width/2, height/2)
        setInTerritory(player.position, player)
    }

    override fun nextTurn() {
        reproduceAndExpand()
        setInTerritory(player.position, player)
        vehicleUses(player.currentVehicle as Vehicle)
        generateNewsItem(randomPosition())
        generateNewsColonies(randomPosition())
    }


    fun setInTerritory(position: Position, icon: Iconizable) {
        territories[position.y][position.x].icons.add(icon)
    }

    override fun gameFinished(): Boolean {
        println("game finish checked")
        return player.livesLeft == 0
    }

    override fun canMoveTo(position: Position): Boolean {
        var canMove = false

        val deltaMoveY = abs(position.y - player.position.y)
        val deltaMoveX = abs(position.x - player.position.x)

        val dist = max(deltaMoveY, deltaMoveX)

        when (player.currentVehicle) {
            is OnFoot -> {
                if (dist < 2) {
                    canMove = true
                }
            }
            is Bicycle -> {
                if (dist < 5) {
                    canMove = true
                }
            }
            is Helicopter -> {
                canMove = true
            }
        }
        return canMove
    }

    override fun moveTo(position: Position, player: Player) {
        territories[player.position.y][player.position.x].icons.remove(player)
        player.position = position
        player.turns++
    }


    override fun takeableItem(): Iconizable? {
        val cell = territories[player.position.y][player.position.x]
        cell.icons.forEach {
            if (it is Item) {
                return it
            }
        }
        return null
    }

    override fun takeItem(item: Item) {
        if (item is Vehicle) {
            player.currentVehicle = item
        } else if (item is Weapon) {
            player.currentWeapon = item
        }
        val cell = territories[player.position.y][player.position.x]
        cell.icons.remove(item)
    }


    fun vehicleUses(vehicle: Vehicle) {
        when (player.currentVehicle) {
            is Bicycle -> {
                vehicle.timesLeft--
                if (vehicle.timesLeft == 0) {
                    player.currentVehicle = OnFoot()
                }
            }
            is Helicopter -> {
                vehicle.timesLeft--
                if (vehicle.timesLeft == 0) {
                    player.currentVehicle = OnFoot()
                }
            }
        }
    }

    fun checkTerritoryItem(position:Position):Boolean{
        var  isInside = false
        territories[position.y][position.x].icons.forEach {
            if(it is Item){
                isInside = true
            }
        }
        return isInside
    }

    private fun generateNewsItem(position: Position) { //position NO lo usamos
        val num = (0..100).random()

        if (!checkTerritoryItem(position)) {
                when (num) {
                    in 0..25 -> {
                        setInTerritory(position, Bicycle())

                    }
                    in 26..35 -> {
                        setInTerritory(position, Helicopter())
                    }


                    in 36..60 -> {
                        setInTerritory(position, Broom())

                    }
                    in 60..70 -> {
                        setInTerritory(position, Sword())
                    }
                }

            }
    }

    fun randomPosition(): Position {
        val y = (0 until width).random()
        val x = (0 until height).random()
        val position = Position(x, y)
        return position
    }

    override fun exterminate() {
        colonies.forEach {
            if (it.colony.size == 0) {
                territories[player.position.y][player.position.x].icons.remove(it.colony)
                colonies.remove(it)
            } else {
                territories[player.position.y][player.position.x].icons.forEach {
                    if (it is Colony) {
                        it.attacked(player.currentWeapon as Weapon)
                    }
                }
            }
        }
    }

    fun checkTerritoryColony(position: Position):Boolean{
        var  isInside = false
        territories[position.y][position.x].icons.forEach {
            if(it is Colony){
                isInside = true
            }
        }
        return isInside
    }


    private fun generateNewsColonies(position: Position) {
        val num = (0..100).random()
        if(!checkTerritoryColony(position)){
            when (num) {
                in 0..10 -> {
                        setInTerritory(position, Dragon())
                        colonies.add(Colonization(Dragon(), position))
                }
                in 11..40 -> {
                        setInTerritory(position, Ant())
                        colonies.add(Colonization(Ant(), position))
                }
            }
        }

    }


    fun generatePositionsAnt(position: Position): List<Position> { //Position (2,3) --> Se representa en el mapa como Position(3,2)
        val listaPositions = mutableListOf<Position>()

        for (y in 0 until width) { //FILA
            for (x in 0 until height) { //COLUMNA
                if (position.y == y && position.x != x) { // Quiero mantener el y=3 -> Tengo que hacer que la position y-> sea fija
                    listaPositions.add(Position(x, position.y)) //SITIOS LIBRES HORIZONTALMENTE
                } else if (position.y != y && position.x == x) {
                    listaPositions.add(Position(position.x,y) //SITIOS LIBRES VERTICAL
                    ) // Quiero mantener el x=2 -> Tengo que hacer que la position x-> sea fija
                }
                /*
                if(y ==3 && x == 2){ // --> Situa el Ant en el mapa : (3,2)
                    setInTerritory(Position(x,y),Ant())
                }*/

            }

        }
        return listaPositions
    }

    fun checkPositionsAndGetRandomPosition(listaPositions: List<Position>): Position {
        val listFreePositions = mutableListOf<Position>()
        for (x in listaPositions.indices) {
            for (y in colonies.indices) {
                if (listaPositions[x] != colonies[y].position) {
                    listFreePositions.add(listaPositions[x])
                }
            }
        }

        //Genero el randomPositions de mi lista de Posiciones Free
        val numberRandom = (0..listFreePositions.lastIndex).random()
        val positionRandom = listFreePositions[numberRandom]

        return positionRandom
    }


    private fun reproduceAndExpand() {
        var positionToExpand: Position
        var colonyToExpand: Colonization

        territories.forEach {
            it.forEach { territori ->
                territori.icons.forEach { icon ->
                    if (icon is Colony) {
                        if (icon.needsToExpand()){
                            colonyToExpand = icon.expand(territori.position)
                            positionToExpand = checkPositionsAndGetRandomPosition(generatePositionsAnt(territori.position))
                            setInTerritory(positionToExpand, colonyToExpand.colony)
                            colonies.add(Colonization(colonyToExpand.colony, positionToExpand))
                        }else {
                            if (icon.willReproduce() && territori.icons.size <3) {
                                icon.reproduce()
                            }
                        }
                    }
                }
            }
        }
    }
}

        /*colonies.forEach {
            if(it.colony.needsToExpand()){
                colonyToExpand = it.colony.expand(it.position)
                positionToExpand = checkPositionsAndGetRandomPosition(generatePositionsAnt(it.position))
                setInTerritory(positionToExpand, colonyToExpand.colony)
                colonies.add(Colonization(colonyToExpand.colony, positionToExpand))
            }else {
                if(it.colony.willReproduce()){
                    it.colony.reproduce()
                }
            }

            }*/








