package theplague.logic

import theplague.interfaces.IPlayer
import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.Vehicles.Bicycle
import theplague.logic.Vehicles.Helicopter
import theplague.logic.Vehicles.OnFoot
import theplague.logic.Weapons.Hand
import theplague.logic.Weapons.Weapon

class Player(
    override var turns: Int = 0,
    override var livesLeft:Int = 15,
    override var currentWeapon: Iconizable = Hand(),
    override var currentVehicle: Iconizable = OnFoot(),
    override val icon: String = "\uD83D\uDEB6"
) :IPlayer {
    override var position:Position = Position(0,0)

}