package theplague.logic.Vehicles

import theplague.interfaces.Position


class Bicycle:Vehicle(){
    override var timesLeft: Int = 6
    override val icon: String = "\uD83D\uDEB2"

    override fun canMove(from: Position, to: Position):Boolean{
        return true
    }
}