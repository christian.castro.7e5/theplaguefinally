package theplague.logic.Vehicles

import theplague.interfaces.Position

class OnFoot: Vehicle() {
    override var timesLeft: Int = -1
    override val icon: String = "\uD83D\uDEB6"

    override fun canMove(from: Position, to: Position): Boolean {
        return true
    }

}