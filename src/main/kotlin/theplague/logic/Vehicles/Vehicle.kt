package theplague.logic.Vehicles


import theplague.interfaces.Position
import theplague.logic.Item

open class Vehicle: Item() {
    override var timesLeft: Int = 0
    override val icon: String = ""


    open fun canMove(from:Position,to:Position):Boolean{
        return true
    }
}