package theplague.logic.Colonies

import theplague.interfaces.Iconizable
import theplague.interfaces.Position
import theplague.logic.Weapons.Broom
import theplague.logic.Weapons.Hand
import theplague.logic.Weapons.Sword
import theplague.logic.Weapons.Weapon
import kotlin.math.abs
import theplague.logic.Colonies.Colonization

sealed class Colony:Iconizable {
    override val icon: String = ""
    //VARIABLE POWER
     var size:Int = 1

    //Icono Primario
    abstract var primaryIcon:String

    abstract val membersToLoseByHand:Int
    abstract val membersToLoseBySword:Int
    abstract val membersToLoseByBroom:Int

    abstract fun willReproduce():Boolean
    fun reproduce(){
         //Tenemos Max 4 bichos por celda
         size++
     }

    fun needsToExpand():Boolean{
        //Aqui indicamos si se expande en otra celda
        return size == 3
    }

    fun attacked(weapon: Weapon) {
        when (weapon) {
            is Hand -> {
                size -= membersToLoseByHand
            }
            is Sword -> {
                size -= membersToLoseBySword
            }
            is Broom -> {
                size -= membersToLoseByBroom
            }
        }

        if (size <= 0) { //REVISAR
            size = 0 //NO puede ser menos
            println("NO hay items")
        }
    }

    //getDeclareConstructor
    fun expand(position:Position):Colonization{
        val colonization:Colonization
        val colony:Colony = this::class.java.getDeclaredConstructor().newInstance()
                // listOf(this).toList()[0];
        colony.size = 1;
        colonization = Colonization(colony,position)
        return colonization
    }

}