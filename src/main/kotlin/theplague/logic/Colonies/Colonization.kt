package theplague.logic.Colonies

import theplague.interfaces.Position

data class Colonization (val colony:Colony,
                         val position:Position){
}