package theplague.logic.Colonies

import theplague.interfaces.Position
import theplague.logic.Weapons.Weapon

class Dragon:Colony() {

    override val icon: String
        get () = primaryIcon.repeat(size)

    override var primaryIcon: String = "\uD83D\uDC09"

    var turns:Int = 0

    override val membersToLoseByHand: Int = 0
    override val membersToLoseBySword: Int = 1
    override val membersToLoseByBroom: Int = 0

    /*
    //if turns ++ % max == 0 -> Se completa un ciclo se reproducoe
    //
    0 % 5 = 0
    1 % 5 = 1
    2 % 5 = 2
    3 % 5 = 0
    4 % 5 = 1
    5 % 5 = 0
     */
    override fun willReproduce(): Boolean {
        val max = 5
        var isReproduce = false

        if(this.size<3){
            if(++turns % max == 0){
                isReproduce = true
            }
        }
        //Comprobar primero y luego incrementar


        return isReproduce
    }
}