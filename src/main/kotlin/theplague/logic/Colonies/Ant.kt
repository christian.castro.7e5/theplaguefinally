package theplague.logic.Colonies

import theplague.interfaces.Position
import theplague.logic.Weapons.Weapon
import kotlin.math.abs

class Ant: Colony() {
    override val icon: String
        get() = primaryIcon.repeat(size)

    override var primaryIcon: String = "\uD83D\uDC1C"

    override fun willReproduce(): Boolean { //Por turno
        //Random puros -> YA si lo quieres sorprender
        var isPossible = false
        val valorRandom = (0..100).random()
        if(this.size <3){
            isPossible = valorRandom <= 30
        }
        return isPossible
    }

    override val membersToLoseByHand: Int = 2
    override val membersToLoseBySword: Int = 1
    override val membersToLoseByBroom: Int = 3

}


