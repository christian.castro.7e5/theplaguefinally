Autors del projecte: Christian Castro i Lluis Vilaró

Els aspectes que hem cosegit complir del projecte és en primer lloc crear la matriu de la llista territori on hem aconseguit col·locar l'icone de jugador i moure'l torn a torn.
També hem conseguit introduir els items (vehicles i armes) dins de la matriu de territori segons el seu percentatge que se li aplica.
A cada un dels items també vam poder aplicar les caracteristiques que composen a cada item, en el cas dels vehicles el seu moviment dins de la matriu i els seus usos limitats, i en el cas de les armes, el mal que fan a cada una de les colonies respectivament.
En quant a les colonies, hem pogut fer que tant les  colonies de formigues com els dracs es generin segons el seu percentatge aleatoriament en el territori.


En els punts que hem de millorar sens dubte es en les funcions que envolten les colonies, també son les que més ens han costat de desenvolupar, sobretot l'expansió i la reproducció, que no  ens funcionaven explicitament com ho indicaba el projecte.

No hi ha capa aspecte que no haguem desenvolupat, ens hem esforat en avançar en tots els fronts que ens proposaba el projecte.